#include "Perso.hpp"
#include "../Graphics/affichage.hpp"
#include <iostream>
Perso::Perso():mColor(1),mPosX(0),mPosY(0),mImage("../ressources/images/perso.png"),mStatue(0){
}

Perso::Perso(int posX,int posY,int color,std::string image,int statue){
	mPosX =posX;
	mPosY =posY;
	mColor = color;
	mImage = image;
	mStatue = statue;

}
int Perso::getPosX() const{
	return mPosX;
}
int Perso::getPosY() const{
	return mPosY;
}
int Perso::getColor() const{
	return mColor;
}
int Perso::getStatue(){
	return mStatue;
}
std::string Perso::getImage(){
	return mImage;
}
void Perso::setStatue(int statue){
	mStatue=statue;
}
void Perso::setImage(std::string image){
	mImage = image;
}
void Perso::findImage(int color){
	std::string image ;
	switch(color){
		case 1:
			image ="../ressources/images/personne/0/1.png";
			break;
		case 2:
			image ="../ressources/images/personne/0/2.png";
			break;
		case 3:
			image ="../ressources/images/personne/0/3.png";
			break;
		case 4:
			image ="../ressources/images/personne/0/4.png";
			break;

	}
	setImage(image);
}
void Perso::setPosX(int x){
	mPosX = x;
}
void Perso::setPosY(int y){
	mPosY = y;
}
void Perso::setColor(int color){
	mColor = color;
}
void Perso::deplacement(int x,int y,std::vector<std::vector <Case*> > &caseLevel,std::vector<Perso*> &perso){
	mOldX =mPosX;
	mOldY = mPosY;
	if(!presenceMur(mPosX/tailleCase,mPosY/tailleCase,x/tailleCase,y/tailleCase,caseLevel)){
		int i = presenceStatue((mPosX+x),(mPosY+y),perso);
		if(i < 5){

			if(((presenceStatue(((mPosX+(2*x))),((mPosY+(2*y))),perso)) >=5) && !(presenceMur((mPosX+(x))/tailleCase,((mPosY+(y))/tailleCase),x/tailleCase,y/tailleCase,caseLevel))){
				mPosX+=x;
				mPosY+=y;
				perso[i]->setPosX(perso[i]->getPosX()+x);
				perso[i]->setPosY(perso[i]->getPosY()+y);


			}
		}else{
			mPosX+=(x);
			mPosY+=(y);

		}

	}
	appuieStatue(perso,caseLevel);
	action(mPosX/tailleCase,mPosY/tailleCase,mOldX/tailleCase,mOldY/tailleCase,caseLevel,perso);


}
int Perso::presenceStatue(int x,int y,std::vector<Perso*> &perso){
	int i = 1;
	while(i<perso.size() &&(perso[i]->getPosX() != x || perso[i]->getPosY() != y) ){
		i++;
	}
	return i;
}
bool Perso::presenceMur(int oldX,int oldY,int deplaceX,int deplaceY,std::vector< std::vector<Case*> > &mur){

	if(deplaceX && deplaceY==0){
			if(deplaceX == 1){
				if(mur[oldX][oldY]->getCase()[3]>0 || mur[oldX+deplaceX][oldY]->getCase()[1]>0){
					return true;
				}
			}else if(deplaceX == -1){
				if(mur[oldX][oldY]->getCase()[1]>0 || mur[oldX+deplaceX][oldY]->getCase()[3]>0){
					return true;
				}
			}
	}else if(deplaceY && deplaceX == 0){
		if(deplaceY == 1){
			if(mur[oldX][oldY]->getCase()[2]>0 || mur[oldX][oldY+deplaceY]->getCase()[0]>0){
				return true;
			}
		}else if(deplaceY == -1){
			if(mur[oldX][oldY]->getCase()[0]>0 || mur[oldX][oldY+deplaceY]->getCase()[2]>0){
				return true;
			}
		}
	}
	return false;
}
void Perso::action(int x,int y ,int oldX,int oldY ,std::vector< std::vector<Case*> > &mur,std::vector<Perso*> &perso){


		if(mur[x][y]->getCase()[4] == BOUTON && perso[0]->getColor() == mur[x][y]->getCase()[5] && !mur[x][y]->getEtatBouton()) {
			mur[x][y]->useButton(mur[x][y]->getCase()[5],mur);
			mur[x][y]->setEtatBouton(true);

		}else if (mur[x][y]->getCase()[4] == BOUTON && perso[0]->getColor() != mur[x][y]->getCase()[5] && mur[x][y]->getEtatBouton()){
			mur[x][y]->useButton(mur[x][y]->getCase()[5],mur);
			mur[x][y]->setEtatBouton(false);

		}else if(mur[x][y]->getCase()[4] == CHANGEUR){
			 perso[0]->setColor(mur[x][y]->getCase()[5]);
			 perso[0]->findImage(mur[x][y]->getCase()[5]);

		 }else if(mur[x][y]->getCase()[4] == TP){
			 int i = 1;
			 while (perso[0]->getColor() != perso[i]->getColor() && i<5){
				 i++;
			 }
			 if(mur[perso[i]->getPosX()/tailleCase][perso[i]->getPosY()/tailleCase]->getCase()[4] == TP){
				 int xtemp = perso[0]->getPosX();
				 int ytemp = perso[0]->getPosY();
				 perso[0]->setPosX(perso[i]->getPosX());
				 perso[0]->setPosY(perso[i]->getPosY());
				 perso[i]->setPosX(xtemp);
				 perso[i]->setPosY(ytemp);
		 }


		}
		if(mur[oldX][oldY]->getCase()[4] == BOUTON && mur[oldX][oldY]->getEtatBouton() && (x!=oldX || y!=oldY)){
			mur[oldX][oldY]->useButton(mur[oldX][oldY]->getCase()[5],mur);
			mur[oldX][oldY]->setEtatBouton(false);
		}
 }

void Perso::appuieStatue(std::vector<Perso*> &perso,std::vector< std::vector<Case*> > &mur){
	for(int i = 1 ;i<5;i++){
		if(mur[perso[i]->getPosX()/tailleCase][perso[i]->getPosY()/tailleCase]->getCase()[4] == BOUTON && !mur[perso[i]->getPosX()/tailleCase][perso[i]->getPosY()/tailleCase]->getEtatBouton()) {
			mur[perso[i]->getPosX()/tailleCase][perso[i]->getPosY()/tailleCase]->useButton(mur[perso[i]->getPosX()/tailleCase][perso[i]->getPosY()/tailleCase]->getCase()[5],mur);
			mur[perso[i]->getPosX()/tailleCase][perso[i]->getPosY()/tailleCase]->setEtatBouton(true);
		}else if(mur[perso[i]->getPosX()/tailleCase][perso[i]->getPosY()/tailleCase]->getCase()[4] != BOUTON){
			mur[perso[i]->getPosX()/tailleCase][perso[i]->getPosY()/tailleCase]->setEtatBouton(false);
		}

	}
}

bool Perso::finJeu(std::vector< std::vector<Case*> > &mur){
	return (mur[mPosX/tailleCase][mPosY/tailleCase]->getCase()[4] == SORTIE);

}














