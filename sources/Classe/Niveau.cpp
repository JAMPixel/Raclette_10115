#include "Niveau.hpp"
#include "../Graphics/affichage.hpp"

Niveau::Niveau():mCase(0), mPerso(0) {
}

Niveau::Niveau(std::string cheminLevel, std::string cheminPerso) {
	std::ifstream level(cheminLevel);
	std::ifstream perso(cheminPerso);
	std::vector<std::vector<Case*> > cases;
	std::vector<Perso*> persos;
	int i, j, k;
	int temp;
	char cheminImage[255];
	std::vector<Case*> tamere;
	
	if(level) {
		for(i=0;i<10;++i) {
			cases.push_back(tamere);
			for(j=0;j<10;++j) {
				cases[i].push_back(new Case());
				cases[i][j]->setX(i*tailleCase);
				cases[i][j]->setY(j*tailleCase);
				for(k=0;k<6;++k){
					level >> temp;
					cases[i][j]->setCase(temp,k);
				} 
				for(k=0;k<4;++k) {
					sprintf(cheminImage, "../ressources/images/case/%d/%d.png", k, cases[i][j]-> getCase()[k]);
					cases[i][j]->setImage(cheminImage,k);
					
				}
				sprintf(cheminImage, "../ressources/images/case/%d/%d.png", cases[i][j]-> getCase()[4], cases[i][j]-> getCase()[5]);
				cases[i][j]->setImage(cheminImage, 4);
			}
		}	
		level.close();
	}
	else {
		std::cout<< "Erreur ouverture du fichier :"  << std::endl;
	}
	mCase=cases;
	
	if(perso) {
		for(i=0;i<5;++i) {
			persos.push_back(new Perso());
			for(j=0;j<4;++j) {
				perso >> temp;
				switch (j) {
					case 0 : persos[i]->setPosY(tailleCase*temp);
					break;
					case 1 : persos[i]->setPosX(tailleCase*temp);
					break;
					case 2 : persos[i]->setColor(temp);
					break;
					case 3 : persos[i]->setStatue(temp);
					break;
				}
				sprintf(cheminImage, "../ressources/images/personne/%d/%d.png", persos[i]->getStatue(), persos[i]->getColor());
				persos[i]->setImage(cheminImage);
			}
		}
		perso.close();
	}
	else {
		std::cout<< "Erreur ouverture du fichier :"  << std::endl;
	}
	mPerso = persos;
}



std::vector<std::vector<Case*> > Niveau::getLevel() const {
	return mCase;
}

std::vector<Perso*> Niveau::getPerso() const {
	return mPerso;
}
