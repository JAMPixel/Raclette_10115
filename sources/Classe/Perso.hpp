#pragma once 
#include <string>
#include "Case.hpp"
#include <vector>


class Perso{
	private:
	
	int mPosX;
	int mPosY;
	int mOldX;
	int mOldY;
	std::string mImage;
	int mColor;
	int mStatue;//0 si perso ,1 si statue
	

	
	
	public:
	Perso(const Perso &) = delete;
	Perso();
	Perso(int,int,int,std::string,int);
	int getPosX() const ;
	int getPosY() const;
	int getColor() const;
	std::string getImage();
	void setPosX(int x);
	void setPosY(int y);
	void setColor(int);
	int getStatue();
	void setStatue(int statue);
	void setImage(std::string image);
	void findImage(int color);
	void deplacement(int,int,std::vector<std::vector <Case*> > &,std::vector<Perso*> &perso);
	bool presenceMur(int,int,int,int,std::vector<std::vector <Case*> > &);
	void action(int x,int y ,int,int,std::vector< std::vector<Case*> > &mur,std::vector <Perso*> &perso);
	int presenceStatue(int x,int y,std::vector<Perso*> &perso);
	void appuieStatue(std::vector<Perso*> &perso,std::vector< std::vector<Case*> > &mur);
	bool finJeu(std::vector< std::vector<Case*> > &mur);
	
	
};
