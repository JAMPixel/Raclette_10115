#include "affichage.hpp"

void freeAffiche(affiche * aff,int choix){
	switch(choix){
		case 0:
			SDL_DestroyRenderer(aff->render);
		case 1 :
			SDL_DestroyWindow(aff->window);

	}
}
void freeCase(std::vector<std::vector <Case*> >  &mur ){
	int taille = mur.size();
	for(int i =0;i<taille;i++){

		for(int j = 0;j<taille;j++){
				delete mur[i][j];


		}
	}
}
void init(affiche *aff,std::vector< std::vector<Case*> > &levelCase){
	SDL_RenderClear(aff->render);
	SDL_Texture *image = 0;
	SDL_Rect coor,coorFond={0,0,tailleCase*10,tailleCase*10};
	SDL_SetRenderDrawColor(aff->render,150,150,150,255);
	SDL_RenderFillRect(aff->render,&coorFond);
	unsigned int taille = levelCase.size();

	for (unsigned int i = 0;i<taille;i++){
		for (unsigned int j = 0;j<taille;j++){
			coor={(int)i*tailleCase,(int)j*tailleCase,tailleCase,tailleCase};

			for (int k = 0; k < 5 ;k++){
				image = IMG_LoadTexture(aff->render,(levelCase[i][j]->getImage()[k]).c_str());


				if(image!=0){
					SDL_RenderCopy(aff->render,image,NULL,&coor);


				}else{
					std::cout <<"erreur  "<< SDL_GetError()<<std::endl;

				}
				SDL_DestroyTexture(image);

			}
		}
	}

}
void affichePerso(affiche* aff,std::vector<Perso*> &perso){
	SDL_Texture* image= 0;
	SDL_Rect coor;
	for(int i =0;i<5;i++){
		coor={perso[i]->getPosX(),perso[i]->getPosY(),tailleCase,tailleCase};

		image= IMG_LoadTexture(aff->render,perso[i]->getImage().c_str());
		if(image!=0){
					SDL_RenderCopy(aff->render,image,NULL,&coor);

		}else{
					std::cout <<"erreur  "<< SDL_GetError()<<std::endl;
		}
		SDL_DestroyTexture(image);

	}

}
